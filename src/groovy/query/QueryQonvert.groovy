package query

import groovy.util.logging.Log4j
import org.springframework.stereotype.Component

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method


@Component
@Log4j
class QueryQonvert {

    static def postRequest() {
        def http = new HTTPBuilder()
        def out = 0
        http.request("https://query.yahooapis.com", Method.GET, ContentType.JSON) { req ->
            uri.path = '/v1/public/yql'
            //yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,EURRUB%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
            uri.query = [q     : 'select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,EURRUB%22',
                         format: 'json',
                         env   : 'store%3A%2F%2Fdatatables.org%2Falltableswithkeys']
            //response.success = successClosure
            response.failure = { resp ->
                log.warn("error ${resp}")
                println("failure")
                println(resp)
            }

            response.success = { resp, json ->
                out = json.text
                println(json)
            }
        }
        out
    }
}
