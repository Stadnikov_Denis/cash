package cashmachine

/**
 * operation to add and get
 */

enum Operation {
    DEBIT,
    CREDIT
}
