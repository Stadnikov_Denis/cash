package cashmachine

/**
 * Possible denomination banknotes
 */
enum Meaning {
    ONE(1),
    /**
     *  Nominal sizes of denominations of 1
     */
            FIVE(5),
    /**
     *  Nominal sizes of denominations of 5
     */
            TEN(10),
    /**
     *  Nominal sizes of denominations of 10
     */
            FIFTY(50),
    /**
     *  Nominal sizes of denominations of 50
     */
            HUNDRED(100),
    /**
     *  Nominal sizes of denominations of 100
     */
            FIVE_HUNDRED(500),
    /**
     *  Nominal sizes of denominations of 500
     */
            ONE_THOUSAN(1000),
    /**
     *  Nominal sizes of denominations of 1000
     */
            FIVE_THOUSAN(5000)
    /**
     *  Nominal sizes of denominations of 5000
     */

    int value

    Meaning(int value) {
        this.value = value
    }
}
