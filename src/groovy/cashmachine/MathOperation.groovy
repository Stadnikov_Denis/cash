package cashmachine

import org.springframework.stereotype.Component

//import org.codehaus.plexus.component.annotations.Component

@Component
class MathOperation {

    /**
     *
     * @param list
     * @param amount
     * @return
     */
    static Boolean existBanknote(Collection<Cash> list, Integer amount) {
        list.each { Cash c ->
            def numberOfNote = c.number
            while (amount > 0 && numberOfNote > 0 && amount >= c.value) {
                amount -= c.value
                numberOfNote--
            }
            if (amount == 0) {
                return
            }
        }
        !amount
    }


    Collection convertAmountToBanknote(Integer amount) {
        def one=0, five=0, ten=0, fifty=0, hundred=0, fiveHundred=0, oneThousan=0, fiveThousan=0

        while (amount != 0) {
            def minus = choiceOfDenomination(amount)
            amount -= minus
            switch (minus) {
                case 5000:
                    fiveThousan++
                    break
                case 1000:
                    oneThousan++
                    break
                case 500:
                    fiveHundred++
                    break
                case 100:
                    hundred++
                    break
                case 50:
                    fifty++
                    break
                case 10:
                    ten++
                    break
                case 5:
                    five++
                    break
                case 1:
                    one++
                    break
            }
        }
        def list =[one, five, ten, fifty, hundred, fiveHundred, oneThousan, fiveThousan]
        list
    }


    Integer choiceOfDenomination(Integer input) {
        Integer output = 0
        switch (input) {
            case input > 5000:
                output = 5000
                break
            case input > 1000:
                output = 1000
                break
            case input > 500:
                output = 500
                break
            case input > 100:
                output = 100
                break
            case input > 50:
                output = 50
                break
            case input > 10:
                output = 10
                break
            case input > 5:
                output = 5
                break
            case input > 1:
                output = 1
                break
        }
        output
    }
}
