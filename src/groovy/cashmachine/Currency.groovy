package cashmachine

/**
 *  The list of possible currencies
 */

enum Currency {

    RUR,
    USD,
    EUR,
    CHF
}
