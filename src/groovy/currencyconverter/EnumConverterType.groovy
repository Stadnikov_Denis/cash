package currencyconverter


enum EnumConverterType {
    RUR_TOFROM_EUR,
    RUR_TOFROM_USD,
    RUR_TOFROM_CHF,
    EUR_TOFROM_USD,
    EUR_TOFROM_CHF,
    USD_TOFROM_CHF,
}
