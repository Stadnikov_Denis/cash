package currencyconverter

class CurrencyConverterEurToUsdImpl extends CurrencyRate implements CurrencyConverter {

    Double currencyRate = 1

    CurrencyConverterEurToUsdImpl(Double currencyRate) {
       super(currencyRate)
    }

    @Override
    Double to(Double inputAmount) {
        inputAmount / currencyRate
    }

    @Override
    Double from(Double inputAmount) {
        inputAmount * currencyRate
    }
}
