package currencyconverter

class CurrencyConverterRurToChfImpl extends CurrencyRate implements CurrencyConverter {


    CurrencyConverterRurToChfImpl(Double currencyRate) {
        super(currencyRate)
    }

    @Override
    Double to(Double inputAmount) {
        inputAmount / currencyRate
    }

    @Override
    Double from(Double inputAmount) {
        inputAmount * currencyRate
    }
}
