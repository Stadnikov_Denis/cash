package currencyconverter

class CurrencyConverterRurToEurImpl extends CurrencyRate implements CurrencyConverter {


    CurrencyConverterRurToEurImpl(Double currencyRate) {
        super(currencyRate)
    }

    @Override
    Double to(Double inputAmount) {
        inputAmount / currencyRate
    }

    @Override
    Double from(Double inputAmount) {
        inputAmount * currencyRate
    }
}
