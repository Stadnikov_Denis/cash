package currencyconverter

class CurrencyConverterRurToUsdImpl extends CurrencyRate implements CurrencyConverter {

    CurrencyConverterRurToUsdImpl(Double currencyRate) {
        super(currencyRate)
    }

    @Override
    Double to(Double inputAmount) {
        inputAmount / currencyRate
    }

    @Override
    Double from(Double inputAmount) {
        inputAmount * currencyRate
    }
}
