package currencyconverter


interface CurrencyConverter {

    Double to(Double inputAmount)

    Double from(Double inputAmount)
}
