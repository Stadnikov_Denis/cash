package currencyconverter


class CurrencyConverterUsdToChfImpl extends CurrencyRate implements CurrencyConverter {

    CurrencyConverterUsdToChfImpl(Double currencyRate) {
        super(currencyRate)
    }

    @Override
    Double to(Double inputAmount) {
        inputAmount / currencyRate
    }

    @Override
    Double from(Double inputAmount) {
        inputAmount * currencyRate
    }
}
