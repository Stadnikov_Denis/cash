package currencyconverter

import org.springframework.stereotype.Component
import javax.annotation.PostConstruct


@Component
class FactoryConvert {

    def exchangeRates = [:] as Map

    @PostConstruct
    void init() {
        exchangeRates = new ConfigSlurper().parse(new File('grails-app/conf/currency/ExchangeRates.groovy').text) as Map
    }

    CurrencyConverter getConverter(EnumConverterType type) {
        switch (type) {
            case EnumConverterType.RUR_TOFROM_EUR:
                return new CurrencyConverterRurToEurImpl(exchangeRates.rur.to.eur)

            case EnumConverterType.RUR_TOFROM_USD:
                return new CurrencyConverterRurToUsdImpl(exchangeRates.rur.to.usd)

            case EnumConverterType.RUR_TOFROM_CHF:
                return new CurrencyConverterRurToChfImpl(exchangeRates.rur.to.chf)

            case EnumConverterType.EUR_TOFROM_USD:
                return new CurrencyConverterEurToUsdImpl(exchangeRates.eur.to.usd)

            case EnumConverterType.EUR_TOFROM_CHF:
                return new CurrencyConverterEurToChfImpl(exchangeRates.eur.to.chf)

            case EnumConverterType.USD_TOFROM_CHF:
                return new CurrencyConverterUsdToChfImpl(exchangeRates.usd.to.chf)

        }
    }
}
