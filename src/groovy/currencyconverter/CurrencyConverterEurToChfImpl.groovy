package currencyconverter

class CurrencyConverterEurToChfImpl extends CurrencyRate implements CurrencyConverter{


    CurrencyConverterEurToChfImpl(Double currencyRate) {
       super(currencyRate)
    }

    @Override
    Double to(Double inputAmount) {
        inputAmount / currencyRate
    }

    @Override
    Double from(Double inputAmount) {
        inputAmount * currencyRate
    }
}
