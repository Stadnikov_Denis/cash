package cashmachine

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.integration.Integration


@Integration
@TestFor(CashService)
//@Mock(Cash)
class CashServiceIntegrationTests extends GroovyTestCase {


    void testPrintCash() {
        def list = service.printCash()
        assertNotNull(list)
    }

    void testAddCash() {
        Cash cash = new Cash(currency: 'RUR', value: 1, number: 10)
        service.addCash(cash, 10, "RUR", 1)
        assert Cash.count == 1

        service.addCash(cash, 10, 'RUR', 1)
        assert Cash.count == 1

        def cash1 = new Cash(currency: 'USD', value: 1, number: 10)
        service.addCash(cash1, 10, 'USD', 1)
        assert Cash.count == 2
    }

    void testGetCash() {
        Cash cash = new Cash(currency: 'RUR', value: 1, number: 10).save()
        service.getCash("RUR", 8)
        assertEquals(Cash.findByCurrency('RUR').number, 2)

        cash = new Cash(currency: 'RUR', value: 100, number: 1).save()
        service.getCash("RUR", 101)
        assertEquals(Cash.findByCurrency('RUR').number, 1)
    }

    void testValidate(){
        Cash cash = new Cash(currency: 'RUR', value: 1, number: 10).save()
        assert service.validate('RUR', 10)
        assert !(service.validate('USD', 10))
        assert !(service.validate('RUR', 11))
    }
}
