package currencyconverter

import org.junit.Before
import org.junit.Test


class FactoryConvertTests extends GroovyTestCase {

    FactoryConvert factoryConvert

    @Before
    public void setUp() throws Exception {
        factoryConvert = new FactoryConvert()
        factoryConvert.init()
    }

    @Test
    void testShouldInit() {
        factoryConvert.init()
        def exchangeRates = factoryConvert.exchangeRates
        assertNotNull exchangeRates
        assertEquals(exchangeRates.rur.to.usd, 70)
        assertEquals(exchangeRates.rur.to.eur, 77)
        assertEquals(exchangeRates.rur.to.chf, 69)
        assertEquals(exchangeRates.eur.to.usd, 1.11)
    }

    @Test
    void testConverter() {
        EnumConverterType rurToUsd = EnumConverterType.RUR_TOFROM_USD
        def a = factoryConvert.getConverter(rurToUsd)
        assertNotNull(a)
    }

    @Test
    void testConvertCurrency(){
        def usd = factoryConvert.getConverter(EnumConverterType.RUR_TOFROM_USD).to(70)
        assertEquals(Math.round(usd), 1)

        def chf = factoryConvert.getConverter(EnumConverterType.USD_TOFROM_CHF).from(5)
        assertEquals(chf, 4.9)
    }

}
