package cashmachine

import cashmachine.MathOperation
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import org.junit.Test
import spock.lang.Specification


/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */

class MathOperationTest {
    @Test
    void testExistBanknote() {
        def cash = new Cash(currency: 'RUR', value: 1, number: 10)
        def list = [cash]
        def a = MathOperation.existBanknote(list, 10)
        assert a
    }

    @Test
    void testExistBanknoteB() {
        def cash = new Cash(currency: 'RUR', value: 1, number: 10)
        def list = [cash]
        def a = MathOperation.existBanknote(list, 11)
        assert !a
    }
}
