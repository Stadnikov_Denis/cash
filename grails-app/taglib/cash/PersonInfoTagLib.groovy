package cash

import cashmachine.UserAccount

class PersonInfoTagLib {
    def info = { attrs, body ->
        def username = UserAccount.findById(1).username
        def msg = g.message(code: "my.viget.numberBancAccInfo.content", args: [username])
        out << msg
    }
}
