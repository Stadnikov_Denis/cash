<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="footerAndHeader"/>
    <title><g:message code="my.title_print_cash.content"/></title>
</head>

<body>
<h1><g:message code="my.print.content"/></h1>

<h2 name="output">${DBc} ${DBv} ${DBn}</h2>

<div id="work">
    <div class="bloc1"><g:each var="sp" in="${cur}" status="i">
        <h2>${sp}</h2>
    </g:each>
    </div>

    <div class="bloc1">
        <g:each var="sp" in="${val}" status="i">
            <h2>${sp}</h2>
        </g:each>
    </div>

    <div class="bloc1">
        <g:each var="sp" in="${num}" status="i">
            <h2>${sp}</h2>

        </g:each>
    </div>
</div>

<h2>${massege}</h2>

<button>
    <a href="index.gsp"><g:message code="my.back.content"/></a>
</button>
</body>
</html>