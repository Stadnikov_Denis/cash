<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="footerAndHeader"/>
    <title><g:message code="my.title_get_cash.content"/></title>
</head>

<body>
<h1><g:message code="my.get.content"/></h1>
<g:form controller="getCash" update="showError">
    <div class="bloc1">
        <g:message code="my.currency.content"/>
        <g:message code="my.amount.content"/>
    </div>

    <div class="bloc1">
        <g:select name="currency" from="${cashmachine.Currency}"/>
        <g:textField value="10" name="number"/>
       <div class="${hasErrors(bean: cashForm,field:'number','error')}">
         <div class ="error">
            <g:renderErrors bean="${cashForm}" as="list" field="number"/>
         </div>
       </div>
    </div>
    <g:actionSubmit value="Отправить" action="debit"/>
</g:form>
<h2>${getmassege}</h2>
<button>
    <a href=""><g:message code="my.back.content"/></a>
</button>
</body>
</html>