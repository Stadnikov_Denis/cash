<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="footerAndHeader"/>
    <title><g:message code="my.title_add_cash.content"/></title>
</head>

<body>
<h1><g:message code="my.add.content"/></h1>

<g:form controller="addCash">
    <div id="work">
        <div class="bloc1">
            <div><g:message code="my.currency.content"/></div>

            <div><g:select name="currency" from="${cashmachine.Currency}"/></div>
        </div>

        <div class="bloc1">
            <div><g:message code="my.value.content"/></div>

            <div><g:select name="value" from="${cashmachine.Meaning?.values()*.value}" valueMessagePrefix="ENUM.Meaning"/></div>
        </div>

        <div class="bloc1">
            <g:message code="my.number.content"/><br>

            <div class="${hasErrors(bean: cashForm, field: 'number', 'error')}">
                <g:textField value="10" name="number"/>
                <div class="error-messages">
                    <g:renderErrors bean="${cashForm}" as="list" field="number"/>
                </div>
            </div>
        </div>
        <div class="bloc1">
            <div>Дата</div>
            <g:textField name="date" value="${formatDate(format:'dd.MM.yyyy', date:new Date())}"/>
        </div>
    </div>

    <g:actionSubmit name="go" value="Отправить" action="credit"/>

</g:form>
<div class="bloc1">
    <h2><g:message code="${message}"/></h2>
    <g:fieldValue bean="${cashForm}" field="date" />
    <button>
        <a href="$(<g:createLink controller='cash' action='index'/>)"><g:message code="my.back.content"/></a>
    </button>
</div>
</body>
</html>