

<%@ page import="cashmachine.BankAccount" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="footerAndHeader"/>
    <title><g:message code="change.account"/></title>
</head>

<body>

<g:form controller="changeAccount">
    <div id="work">
        <div class="bloc1">
            <div>
                <g:message code="change.account"/>
                <div><g:select name="id" from="${listAccount}" optionValue="currency"
                               optionKey="id"/></div>
            </div>
            <g:actionSubmit name="go" value="Сменить" action="сhange"/>
        </div>
    </div>
</g:form>

</body>
</html>