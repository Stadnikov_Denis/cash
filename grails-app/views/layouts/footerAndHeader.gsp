<!DOCTYPE html>

<head>
    <title><g:layoutTitle default="Grails"/></title>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="grails-datatables.css"/>
    <asset:stylesheet src="grails-datatables-plain.css"/>
    <asset:javascript src="grails-datatables.js"/>

    <g:javascript library="jquery" plugin="jquery"/>

    <g:layoutHead/>
</head>

<body>
<g:render template="/layouts/header"></g:render>
<div class="bread">
    <crumbs:breadcrumbs/>
</div>

<g:layoutBody/>
<g:render template="/layouts/footer"></g:render>
</body>
</html>