<%@ page import="cashmachine.BankAccount" %>
<div id="grailsLogo" role="banner">
    <div id="logo">
        <g:link controller="Cash" action="index"><asset:image src="bank.png" alt="Grails" height="80"/></g:link>

        <div id="flags">
            <langs:selector langs="ru, en" default="ru"/>
            <button><g:message code="my.button.content"/></button>
        </div>

        <div id="inform">
            <div><g:message code="my.viget.bancAccInfo.content"/></div>

            <div><g:info/> ${numberAcc}</div>

            <div><g:message code="my.viget.balance.content"/> ${balance} ${curAc}</div>

        </div>
    </div>
</div>