<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="footerAndHeader"/>
    <title><g:message code="my.title_main_page.content"/></title>
</head>

<body>
<h1><g:message code="my.hello.content"/><br></h1>

<div id="button_content">
    <g:link controller="Cash" action="addCash">
        <button name="add" class="BunkButton">
            <g:message code="my.add.content"/>
        </button></g:link>

    <a href="GetCash">
        <button name="get" class="BunkButton">
            <g:message code="my.get.content"/>
        </button></a><br>

    <a href="">
        <button name="print" class="BunkButton">
            <g:message code="my.print.content"/>
        </button></a>

    <a href="Journal">
        <button name="journal" class="BunkButton">
            <g:message code="page.journal"/>
        </button>
    </a>

    <a href="http://localhost:8080/Cash/ChangeAccount/index">
        <button name="сhangeAccount" class="BunkButton">
            <g:message code="change.account"/>
        </button>
    </a>

</div>
</body>
</html>