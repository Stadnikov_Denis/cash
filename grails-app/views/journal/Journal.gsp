
<%@ page import="cashmachine.BankAccount" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="footerAndHeader"/>
    <title></title>
</head>

<body>
<h1>На счете: ${bankAccAmount}</h1>
<dt:datatable name="Journal" dataItems="${table}">
    <dt:column name="currency" title="Валюта"/>
    <dt:column name="date" type="date" title="Дата"/>
    <dt:column name="operation" title="Операция"/>
    <dt:column name="sum" title="Сумма"/>
</dt:datatable>
<asset:deferredScripts/>
</body>
</html>