package cashmachine

import forms.GetCashForm

class GetCashController {
    def cashService
    def journalService
    def bankAccountService

    def index = {
        render view: 'getCash', model: [curr     : Currency, balance: bankAccountService.balance,
                                        curAc    : bankAccountService.currencyAccount(),
                                        numberAcc: bankAccountService.numberAccount()]
    }

    def debit = {
        GetCashForm cashForm ->
            if (bankAccountService.balanceInfo(cashForm.number, cashForm.currency)) {
                bankAccountService.getToBankAccount(cashForm.number, cashForm.currency)//списываем со счета
                render(view: 'GetCash', model: [getmassege: 'OK',
                                                balance   : bankAccountService.balance,
                                                cashForm  : cashForm,
                                                curAc     : bankAccountService.currencyAccount(),
                                                numberAcc : bankAccountService.numberAccount()])
                journalService.getBankAccount(cashForm.number, cashForm.currency)
            } else {
                render(view: 'GetCash', model: [getmassege: 'ERROR',
                                                balance   : bankAccountService.balance,
                                                cashForm  : cashForm,
                                                curAc     : bankAccountService.currencyAccount(),
                                                numberAcc : bankAccountService.numberAccount()])
            }
    }

    def getCash() {}
}
