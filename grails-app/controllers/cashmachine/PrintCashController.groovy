package cashmachine

class PrintCashController {

    def cashService
    def bankAccountService

    def index() {

        def allData = cashService.printCash()
        render(view: 'PrintCash', model: [cur      : allData.currency,
                                          val      : allData.value,
                                          num      : allData.number, massege: 'OK',
                                          balance  : bankAccountService.balance,
                                          curAc    : bankAccountService.currencyAccount(),
                                          numberAcc: bankAccountService.numberAccount()])
    }

}
