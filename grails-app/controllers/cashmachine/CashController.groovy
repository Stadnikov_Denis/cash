package cashmachine

class CashController {

    def cashService
    def bankAccountService

    def SelectAccount() {
        return [balance    : bankAccountService.balance,
                curAc      : bankAccountService.currencyAccount(),
                listAccount: BankAccount.list() as List,
                numberAcc  : bankAccountService.numberAccount()]
    }

    def index() {
        if (params.id) {
            bankAccountService.changeAccount(params.id as Integer)
        }
        println(bankAccountService.getApiData())
        render view: 'index', model: [balance  : bankAccountService.balance,
                                      curAc    : bankAccountService.currencyAccount(),
                                      numberAcc: bankAccountService.numberAccount()]
    }

    def addCash = { redirect controller: 'AddCash' }

    def getCash = { redirect controller: 'GetCash' }

    def printCash = { redirect controller: 'PrintCash' }

    def journal = { redirect controller: 'Journal' }

    def сhangeAccount = { redirect controller: 'ChangeAccount' }


}