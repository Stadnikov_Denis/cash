package cashmachine

import forms.AddCashForm

class AddCashController {

    def cashService
    def journalService
    def bankAccountService

    def index = {
        render(view: 'addCash', model: [balance  : bankAccountService.balance,
                                        curAc    : bankAccountService.currencyAccount(),
                                        numberAcc: bankAccountService.numberAccount()])
    }

    def credit = {
        AddCashForm cashForm ->
            if (cashForm.validate()) {
                bankAccountService.addToBankAccount(cashForm.value, cashForm.number, cashForm.currency)
                //добавляем сумму на счет
                journalService.addBankAccount(cashForm.value, cashForm.number, cashForm.currency)
            }
            render(view: 'addCash', model: [message  : 'my.good.content',
                                            balance  : bankAccountService.balance,
                                            cashForm : cashForm,
                                            mytest   : cashForm.date,
                                            curAc    : bankAccountService.currencyAccount(),
                                            numberAcc: bankAccountService.numberAccount()])
    }

    def addCash() {
        render(view: 'addCash')
    }
}
