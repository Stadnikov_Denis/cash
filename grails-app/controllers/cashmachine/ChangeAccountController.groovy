package cashmachine


class ChangeAccountController {

    def bankAccountService

    def index() {
        def listAccount = BankAccount.list() as List
        render view: 'ChangeAccount', model: [balance    : bankAccountService.balance,
                                              listAccount: listAccount,
                                              curAc      : bankAccountService.currencyAccount(),
                                              numberAcc  : bankAccountService.numberAccount()]
    }

    def сhange(params) {
        bankAccountService.changeAccount(params.id as Integer)

        def listAccount = BankAccount.list() as List
        render(view: 'ChangeAccount', model: [balance    : bankAccountService.balance,
                                              listAccount: listAccount,
                                              curAc      : bankAccountService.currencyAccount(),
                                              numberAcc  : bankAccountService.numberAccount()])
    }
}
