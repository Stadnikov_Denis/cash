package cashmachine

class JournalController {

    def journalService
    def bankAccountService

    def index() {
        render(view: 'Journal', model: [bankAccAmount: bankAccountService.balance,
                                        balance      : bankAccountService.balance,
                                        table        : journalService.journalRecordInfo,
                                        curAc        : bankAccountService.currencyAccount(),
                                        numberAcc    : bankAccountService.numberAccount()
        ])
    }

}
