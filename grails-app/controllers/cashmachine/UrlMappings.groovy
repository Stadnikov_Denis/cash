package cashmachine

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "Cash", action: "SelectAccount")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
