import editor.CustomPropertyEditorRegistrar

// Place your Spring DSL code here
beans = {
    customPropertyEditorRegistrar(CustomPropertyEditorRegistrar)

    menuDefinitionServiceProxy(org.springframework.aop.scope.ScopedProxyFactoryBean) {
        targetBeanName = 'menuDefinitionService'
        proxyTargetClass = true
    }
}
