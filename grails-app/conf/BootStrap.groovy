import cashmachine.BankAccount
import cashmachine.Currency
import cashmachine.UserAccount

class BootStrap {

    def init = { servletContext ->

        new BankAccount(id: 1, amount: 25, currency: Currency.USD, userId: 1).save()
        new BankAccount(id: 2, amount: 500, currency: Currency.RUR, userId: 1).save()
        new BankAccount(id: 3,amount: 30, currency: Currency.CHF, userId: 1).save()

        new UserAccount(id: 1, login: "lorem", password: "ipsum", username: "Leto").save()
        println(UserAccount.findById(1).username)
    }
    def destroy = {
        BankAccount.findByCurrency(Currency.USD).delete()
        BankAccount.findByCurrency(Currency.RUR).delete()
        BankAccount.findByCurrency(Currency.CHF).delete()
    }
}
