package cashmachine

/**
 * счет
 */
class BankAccount {

    static belongsTo = UserAccount
    static hasMany = [operations: JournalRecord]

    Integer id
    Integer userId
    Double amount//сколько всего на счете
    Currency currency

    static constraints = {

    }
}
