package cashmachine

class UserAccount {

    static hasMany = [bankAccount: BankAccount]

    Integer id
    String login
    String password
    String username

    static constraints = {
    }
}
