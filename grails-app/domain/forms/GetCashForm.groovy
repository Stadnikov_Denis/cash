package forms


class GetCashForm {
    String currency
    Integer number

    static constraints = {
        number blank: false, nullable: false
    }
}

