package forms


class AddCashForm {

    String currency
    Integer value
    Integer number
    Date date

    static constraints = {
        number blank: false, nullable: false
    }


}
