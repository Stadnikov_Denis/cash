package cashmachine

import breadcrumbs.MenuItem
import grails.transaction.Transactional

@Transactional
class MenuDefinitionService {

    static transactional = false

    static scope = "session"

    static proxy = true


    def loadMenuDefinition() {
        def menus = []
        MenuItem menuThreeTwo = new MenuItem(name : "page-two-", message:"page.home", controller: "Cash", action:"index")
        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.add", controller: "Cash", action:"addCash")

        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.add", controller: "AddCash", action:"index")
        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.add", controller: "AddCash", action:"credit")

        menuThreeTwo << new MenuItem(name : "page-two.two", message:"page.get", controller: "Cash", action:"getCash")

        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.add", controller: "GetCash", action:"index")
        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.add", controller: "GetCash", action:"debit")

        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.print", controller: "Cash", action:"printCash")
        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.journal", controller: "Cash", action:"journal")

        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.change", controller: "ChangeAccount", action:"index")
        menuThreeTwo << new MenuItem(name : "page-two.one", message:"page.change", controller: "ChangeAccount", action:"сhange")
        menus << menuThreeTwo

        menus
    }
}
