package cashmachine

import grails.transaction.Transactional

@Transactional
class JournalService {


    def addBankAccount(value, number, currency) {
        def amount = value * number
        new JournalRecord([currency: currency, date: new Date(), operation: Operation.DEBIT, sum: amount]).save()
    }

    def getBankAccount(amount, currency) {
        new JournalRecord([currency: currency, date: new Date(), operation: Operation.CREDIT, sum: amount]).save()
    }

    def getBankAccountInfo() {
        BankAccount.list(max: 15)
    }

    def getJournalRecordInfo() {
        JournalRecord.list().reverse()
    }
}
