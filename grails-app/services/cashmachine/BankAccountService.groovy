package cashmachine

import currencyconverter.EnumConverterType
import currencyconverter.FactoryConvert
import query.QueryQonvert

/**
 * Действия связанные со счетом
 */

class BankAccountService {

    Integer bankAccountId = 2

    static transactional = true

    FactoryConvert factoryConvert

    void addToBankAccount(value, number, currency) {
        def amount = converterResult(value * number, currency)
        def list = BankAccount.findByCurrency(currency as Currency)

        if (list) {
            def update = BankAccount.get(list.id)
            update.amount = list.amount + amount
            update.save()
        } else {
            new BankAccount([amount: amount, currency: currency]).save()
        }
    }

    void getToBankAccount(sum, currency) {
        def amount = converterResult(sum, currency)
        def list = BankAccount.findByCurrency(currency as Currency)
        def update = BankAccount.get(list.id)

        update.amount = list.amount - amount
        update.save()
    }

    Double getBalance() {
        def list = BankAccount.findById(bankAccountId).amount as List
        Double balance = (Double) list?.sum { it }
        balance
    }

    Boolean balanceInfo(amount, currency) {
        converterResult(amount, currency) <= balance
    }

    Double converterResult(Integer amount, String currency) {
        def result = amount
        def currencyAccount = BankAccount.findById(bankAccountId).currency
        def converterName = "${currencyAccount}_TOFROM_${currency}"

        if (Currency.valueOf(currency) != currencyAccount) {
            result = factoryConvert.getConverter(EnumConverterType.valueOf(converterName)).from(amount)
        }
        result
    }

    void changeAccount(Integer id) {
        this.bankAccountId = id
    }

    String currencyAccount() {
        BankAccount.findById(bankAccountId).currency
    }

    String numberAccount(){
        this.bankAccountId
    }

    def getApiData() {
        QueryQonvert.postRequest()
    }
}
