package cashmachine

/**
 * Действия связанные с банкоматом
 */

class CashService {
    static transactional = true

   /* def addCash(cash, int number, String currency, int value) {//положить купюры в банкомат
        boolean save = true
        def list = cash.createCriteria().get {
            and {
                eq('currency', currency)
                eq('value', value)
            }
        }
        if (list) {
            def update = cash.get(list.id)
            update.number = list.number + number
            update.save()
            save = false
        }
        if (save) {
            cash.save([currency: currency, value: value, number: number])
        }
    }

    def printCash() {//выдет коллиичество купюр в банкомате и их номинал
        Cash.createCriteria().list {
            order('currency', 'asc')
            order('value', 'asc')
        } as List
    }

    def validate(currency, amount) {//проверка, можем ли мы выдать купюры или их нет
        def list = Cash.createCriteria().list {
            eq('currency', currency)
            order('value', 'asc')
        } as List
        MathOperation.existBanknote(list, amount)
    }

    def getCash(currency, amount) {//выдет купюры
        def list = Cash.createCriteria().list {
            eq('currency', currency)
            order('value', 'asc')
        } as List
        list.each { Cash c ->
            def listUp = c.number, numbersOfNote = 0
            while (amount > 0 && listUp > 0 && amount >= c.value) {
                amount -= c.value
                listUp--
                numbersOfNote++
                def id = Cash.findAllByCurrencyAndValue(currency, c.value)
                def update = Cash.get(id.id)
                update.number = listUp
                update.save()
            }
            deleteBanknote()
        }
    }

    def deleteBanknote() {
        def id = Cash.createCriteria().get {
            eq('number', 0)
        }
        if (id) {
            def delete = Cash.get(id.id)
            delete.delete()
        }
    }*/

}
